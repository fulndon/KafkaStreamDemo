package ins.framework.kafka.assignpartion;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

public class ProducerStudy {
    public static void main(String[] args) {
        String topic = "test6";
     //   topic = "test";
        Producer<String, String> producer = new KafkaProducer<String, String>(getConfig());
        for (int i = 0; i < 1; i++)
            //第一种分区策略，手动指定分区，让数据进入到我们指定的分区里面去
//            producer.send(new ProducerRecord<String, String>("postgres.test0814",1,"partition_1","指定partition"+Integer.toString(i)));

            //第二种分区策略，通过key的hash取模来进行计算我们数据的分区，如果使用这种方式来存储，一定要注意，key值一定要变，如果不变就会造成数据的热点问题，也就是数据倾斜问题

//            producer.send(new ProducerRecord<String, String>(topic,"好的"+i,"好的sss"));

            //第三种分区策略，通过轮询，实现数据的轮流发送到各个partition当中去
            producer.send(new ProducerRecord<String, String>(topic, "helllo"+Integer.toString(i)));

        producer.close();
    }
    public static Properties getConfig(){

        Properties props = new Properties();
//        props.put("bootstrap.servers", "10.10.1.7:9092,10.10.1.8:9092,10.10.1.10:9092");
        props.put("bootstrap.servers", "10.10.68.200:9092,10.10.68.201:9092,10.10.68.199:9092");
//        props.put("bootstrap.servers", "zk1:9092,zk2:9092,zk3:9092");
//        props.put("bootstrap.servers", "192.168.56.128:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);

//        props.put("security.protocol", "SASL_PLAINTEXT");
//        props.put("sasl.mechanism", "PLAIN");
//       // props.put("sasl.jaas.config", "PLAIN");
//        System.setProperty("java.security.auth.login.config",
//                "E:test\\kafka_client_jaas.conf"); //配置文件的路径
        //自定义分区类
       // props.put("partitioner.class", "ins.framework.kafka.assignpartion.MyPartitioner");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return props;
    }
}
