package ins.framework.kafkastream;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;

import java.util.HashMap;
import java.util.Map;

public class CommonUtil {
    public static Map<String, Object> connection(String groupName, String ip) {
        Map<String, Object> properties = new HashMap<>();
        // 指定一个应用ID，会在指定的目录下创建文件夹，里面存放.lock文件
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, groupName);
        // 指定kafka集群
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, ip+":9092");
        // 指定一个路径创建改应用ID所属的文件
        properties.put(StreamsConfig.STATE_DIR_CONFIG, "E:\\kafka-stream");
        // key 序列化 / 反序列化
        properties.put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // value 序列化 / 反序列化
        properties.put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        return properties;
    }
}
